Ein einfaches Kontaktformular als Modul für Joomla 4.

# Funktionen
Die Hauptfunktionen dieses Moduls sind:
* Ein Kontaktformular mit automatischer Danke-Nachricht (Text frei konfigurierbar).
* Standarttext des Formulars kann vordefiniert werden.
* Frei konfigurierbares Auswahlformular für häufige Anliegen.
* Pro Anliegen können Antwortnachricht an den Sender, sowie die Adresse des Empfängers frei gewählt werden.
* CSS Definitionen können hinzugefügt werden.
* Möglichkeit für Google unabhänige Duplikatskontrolle und Schutz vor Bots.

# Druckfenster-Template
Wird das Auswahlformular eingeschaltet ist folgende CSS-Definition unter Design sinnvoll.
```
select {
	height: 35px;
}
```
# Abhänigkeiten
Graita-Mira Framework
https://gitlab.com/gratia-mira-opensource/gratia-mira-framework