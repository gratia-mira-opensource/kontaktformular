# 1.1.3 08.11.2024
* Verbesserung: Formulare sollten beim neu laden nicht nochmals abgesendet werden können.
# 1.1.2 14.06.2023
* Neue Funktion: Möglichkeit für ein Kontrollwort.
* Verbesserung: Google oder interne Prüfung.
# 1.1.1 15.05.2023
* Neue Funktion: Die Google-Formulareingabe wird nun nicht mehr via Google ReCaptcha geprüft, sondern intern.
* Fehler: Absendername wird nur korrekt verarbeitet.
# 1.1.0 13.05.2023
Neue Funktion: Formularausgabe  
Neue Funktion: Absenderinformationen können überschreiben werden.  
Verbesserung: Layout-Verbesserung Admin-Benachrichtigung.   
# 1.0.1 18.04.2023
Neue Funktion: Automatisches Dankes-E-Mail.  
Verbesserung: Mehr Einstellmöglichkeiten.   
# 1.0.0 03.04.2023
Erste Version freigegeben.   