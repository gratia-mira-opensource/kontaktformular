<?php
defined('_JEXEC') or die;

/**
 * Fehlerausgabe Framework
 * 
 * @since 0.0.2 (Framework)
 */

if(defined('GMFramework')) {
    $Aufgabe = 'aktualisieren';
} else {
    $Aufgabe = 'installieren oder <a href="https://docs.joomla.org/Help4.x:Plugins:_Name_of_Plugin/de" title="zur Joomla-Hilfeseite" target="_blank">aktivieren</a>';
}
?>
Bitte <a href="https://gitlab.com/gratia-mira-opensource/gratia-mira-framework/-/releases" title="zum Framework" target="_blank">GMFramework <?php echo $Aufgabe; ?></a>!