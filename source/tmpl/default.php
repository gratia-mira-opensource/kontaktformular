<?php
// No direct access
defined('_JEXEC') or die;

/**
 * Kontaktformular Standartlayout
 * @since 1.0.0
 * @version 1.1.2
 */
?>

<div class="kontaktformular">

    <form id="Formular" class="Kontaktformular" method="post" action="">


        <div class="formular-gruppe">
            <label for="Name" class="kontaktformular-label">Name*</label>
            <div class="kontaktformular-eingabe">
                <input type="text" id="Name" name="Name" class="kontaktformular-feld" required>
            </div>
        </div>

        <div class="formular-gruppe">
            <label for="Firma" class="kontaktformular-label">Firma</label>
            <div class="kontaktformular-eingabe">
                <input type="text" id="Firma" name="Firma" class="kontaktformular-feld">
            </div>
        </div>

        <div class="formular-gruppe">
            <label for="Email" class="kontaktformular-label">Ihre E-Mailadresse*</label>
            <div class="kontaktformular-eingabe">
                <input type="email" id="Email" name="Email" class="kontaktformular-feld" required>
            </div>
        </div>

        <div class="formular-gruppe">
            <label for="Telefon" class="kontaktformular-label">Ihre Telefonnummer</label>
            <div class="kontaktformular-eingabe">
                <input type="tel" id="Telefon" name="Telefon" class="kontaktformular-feld">
            </div>
        </div>

        <?php echo $oKontaktformular->Auswahlformular; ?>

        <div class="formular-gruppe">
            <label for="Nachricht" class="kontaktformular-label">Ihre Nachricht*</label>
            <div class="kontaktformular-eingabe">
                <textarea id="Nachricht" name="Nachricht" class="kontaktformular-feld" rows="10" maxlength="3000" wrap="physical" required><?php if($params->get('StandartNachricht')) { echo $params->get('StandartNachricht'); } ?></textarea>
            </div>
        </div>
        <?php 
            echo $params->get('Sicherheit','', 'Integer') ? GMF_Sicherheit::initialisieReCaptcha() : GMF_Sicherheit::initialisieFormularsicherheit();
        
            if($params->get('Kontrollwort','','String')) { 
                echo 
                '<div class="formular-gruppe">
                    <label for="Kontrollwort" class="kontaktformular-label">Kontrollwort*</label>
                    <div class="kontaktformular-eingabe">
                    <input type="text" id="Kontrollwort" name="Kontrollwort" class="kontaktformular-feld" required>
                    </div>
                </div>
                Geben Sie zur Sicherheit oben folgenden Wert ein: <code>' . $params->get('Kontrollwort','','String') . '</code>';
            }
        ?>
        <div class="formular-gruppe">
            <button type="submit" id="kontaktformular-button">Nachricht senden</button>
        </div>

    </form>

</div>