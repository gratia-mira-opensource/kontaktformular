<?php
/**
 * Klasse für das Kontaktformular! 
 * @since 1.0.0
 * @version 1.1.1
 * 
 */
 // Kein direkter Aufruf der PHP-Datei
defined('_JEXEC') or die('Restricted Access');

// Hilfsklasse
use Joomla\CMS\Factory as Factory;




class Kontaktformular {

	/** @var string Name des Senders (zwingend). */
	public $Name; 
	/** @var string Name der Firma (optional). */
	public $Firma; 
	/** @var string Email des Senders (zwingend). */
	public $Email; 
	/** @var string Telefon des Senders (optional). */
	public $Telefon; 
	/** @var string Nachricht (zwingend). */
	public $Nachricht; 
	/** @var string Das vom Benutzer ausgewählte Feld | Gebiet */
	public $Auswahlfeld;
	
	/** @var string HTML-Code des Auswahlformulars (falls vorhanden zwingend) */
	public $Auswahlformular;
	
	/**
	 * Die Klasse instanziieren, d.h. ein Objekt erzeugen
	 *
	 * @param [type] $paramsn Einstellungen
	 * @version 1.1.2
	 */ 
	function __construct(&$params) {

		$doc = Factory::getDocument(); ?>

		<?php 	
		/** @var string  Wichtige CSS Definitionen */
		$doc->addStyleDeclaration($params->get('EigenesCSS'));

		// Aktiviert die Hilfsklassen für URL-Parameter
		$get = Factory::getApplication();
		$Eingabe = $get->input;
		
		$this->erstelleAuswahlformular($params);


		if ($_SERVER['REQUEST_METHOD'] == 'POST') {
			
			// Liest die Parameter aus, welche nur zum Steuern des Moduls notwendig sind.
			$this->Name = trim($Eingabe->get('Name','', 'String'));
			$this->Firma = trim($Eingabe->get('Firma','', 'String'));
			$this->Email = trim($Eingabe->get('Email','', 'String'));
			$this->Telefon = trim($Eingabe->get('Telefon','', 'String'));
			$this->Nachricht = trim($Eingabe->get('Nachricht','', 'String'));
			$this->Auswahlfeld = trim($Eingabe->get('Auswahlfeld','', 'Integer'));

			if($params->get('Kontrollwort','','String') == $Eingabe->get('Kontrollwort','', 'String')) {
				$MenschlicheEingabe =  $Eingabe->get('Sicherheit','', 'Integer') ? GMF_Sicherheit::pruefeReCaptcha() : GMF_Sicherheit::pruefeFormularsicherheit(true,array($this->Name,$this->Firma,$this->Email,$this->Telefon,$this->Nachricht,$this->Auswahlfeld));

				if($MenschlicheEingabe) {
					$this->sendeMail($this, $params);
				} else {
					Factory::getApplication()->enqueueMessage('Diese Anfrage wurde schon verarbeitet!', 'error');
				}
			} else {
				Factory::getApplication()->enqueueMessage('Geben Sie bitte das richtige Kontrollwort ein!', 'error');
			}
		}
    }
	

	/**
	 * Stellt den HTML-Code des Auswahlfeldes zusammen
	 *
	 * @param object $Daten Die Einstellungen des Moduls
	 * @return object Stellt das Objekt für die Formularausgabe des Auswahlfelds zur Verfügung
	 * @since 1.1.0
	 */
	private function erstelleAuswahlformular(object $Daten) {
		
		// print_r($Daten);
		
		if($Daten->get('Auwahlfeld')) {
			$this->Auswahlformular = 
										'<div class="formular-gruppe">
											<label for="kontaktformular-auswahlfeld" class="kontaktformular-label">'. $Daten->get('AuwahlfeldNamen') . '*</label>
											
											<select name="Auswahlfeld" id="kontaktformular-auswahlfeld">';

												$i = 0;
												foreach($Daten->get('AuswahlfeldOptionen') as $Option) {
													$this->Auswahlformular .= '<option value="'. $i . '">'. $Option->AuswahlvarianteName . '</option>';
													$i++;
												}
			$this->Auswahlformular .=
											'</select>
										</div>';
		} else {
			$this->Auswahlformular = '';
		}

	}

	/**
	 * Sendet das Mail
	 *
	 * @param   object  $Objekt         Die Angaben vom Benutzer
	 * @param   object  $Einstellungen  Die Einstellungen vom Administrator
	 *
	 * @return void
	 * @throws \PHPMailer\PHPMailer\Exception
	 * @since   1.0.0
	 * @version 1.1.3
	 * @link    https://docs.joomla.org/Display_error_messages_and_notices
	 * @link    https://docs.joomla.org/Sending_email_from_extensions
	 */
	private function sendeMail(object $Benutzereingabe,object $Einstellungen) {	

		$Auswahlfelder = $Einstellungen->get('Auwahlfeld');
		
		// String zum Abrufen von Informationen abrufen
		$AuswahlArray = $Einstellungen->get('AuswahlfeldOptionen');
		$Key = 'AuswahlfeldOptionen' .$Benutzereingabe->Auswahlfeld;

		// Liest die Einstellungen aus dem xml (Joomla Backend) aus
		/** @var string Der Name der gewählten Auswahl Variante */
		$AuswahlvarianteName = $Auswahlfelder &&  $AuswahlArray->$Key->AuswahlvarianteName ? $AuswahlArray->$Key->AuswahlvarianteName : '';
		/** @var string Empfängeradresse. Wird im Joomla Backend eingestellt. */
		$Empfaenger = $Auswahlfelder && !empty($AuswahlArray->$Key->AuswahlEmpfaenger) ? trim($AuswahlArray->$Key->AuswahlEmpfaenger) : trim($Einstellungen->get('Empfaenger'));
		/** @var string Wird dem Benutzer eine Dankesnachricht übermittelt. (Ja = 1; Nein = 0) */
		$NachrichtBetreff = $Auswahlfelder ? $AuswahlArray->$Key->AuswahlNachrichtPrefix . $AuswahlvarianteName : trim($Einstellungen->get('NachrichtBetreff'));
		/** @var string Betreff der Danke-Nachricht an den Fragesteller. Wird im Joomla Backend eingestellt. */
		$DankeNachrichtStatus = trim($Einstellungen->get('DankeNachrichtStatus'));
		/** @var string Danke-Nachricht an den Fragesteller. Wird im Joomla Backend eingestellt. */
		$DankeNachrichtBetreff = trim($Einstellungen->get('DankeNachrichtBetreff'));
		/** @var string Stehen Administrator-Definierte Fehler zur Verfügung, wird hier die Auswahl vom Benutzer zurückgegeben  */
		$DankeNachricht = $Auswahlfelder && !empty($AuswahlArray->$Key->AuswahlDankeNachricht) ? $AuswahlArray->$Key->AuswahlDankeNachricht : trim($Einstellungen->get('DankeNachricht'));

		// Den Sender auswählen
		$config = Factory::getConfig();
		
		
		$AbsenderMail = empty($Einstellungen->get('SenderMail')) ? $config->get('mailfrom') : trim($Einstellungen->get('SenderMail'));
		$AbsenderName = empty($Einstellungen->get('SenderName')) ? $config->get('fromname') : trim($Einstellungen->get('SenderName'));
	

		$Sender = array( 
			$AbsenderMail,
			$AbsenderName);

		// Mailfunktion
		$mailer = Factory::getMailer();

		$mailer->setSender($Sender);
		
		$mailer->addRecipient($Empfaenger);
		$mailer->addReplyTo($Benutzereingabe->Email,$Benutzereingabe->Name);
		
		/** @todo Diese Einstellung aus dem Konfiguration nehmen, ansonsten "Eine Anfrage über Webseite" oder Eine Anfrage von Name */
		$mailer->setSubject($NachrichtBetreff);
		
		/** @var string Der Inhalt des Mails */
		$Inhalt = '<h3>Anfrage von ' . str_replace('www.','',$_SERVER['SERVER_NAME']) . '</h3>';
		$Inhalt .= 'Name: ' . $Benutzereingabe->Name . '<br>';
		$Inhalt .= 'Firma: ' . $Benutzereingabe->Firma . '<br>';
		$Inhalt .= 'E-Mail: ' . $Benutzereingabe->Email . '<br>';
		$Inhalt .= 'Telefon: ' . $Benutzereingabe->Telefon . '<br>';
		$Inhalt .= $Auswahlfelder ? $Einstellungen->get('AuwahlfeldNamen') . ': ' . $AuswahlvarianteName . '<br><br>' : '<br>';
		$Inhalt .= '<hr><br>';
		$Inhalt .= nl2br($Benutzereingabe->Nachricht);

		$mailer->isHtml(true);
		$mailer->Encoding = 'base64';
		$mailer->setBody($Inhalt);
		$Senden = $mailer->Send();
		
		// Erfolg oder Misserfolg zurückgeben.
		if ($Senden !== true ) {
			Factory::getApplication()->enqueueMessage('Leider ist ein Fehler aufgetreten. Versuchen Sie es einfach noch einmal!', 'error');
		} else {
			Factory::getApplication()->enqueueMessage('Anfrage erfolgreich gesendet!', 'message');
		}

		if($DankeNachrichtStatus) {
			$Dankesmail = Factory::getMailer();

			$Dankesmail->setSender($Sender);

			$Dankesmail->setSubject($DankeNachrichtBetreff);
			$Dankesmail->addRecipient($Benutzereingabe->Email);
			$DankeInhalt = $DankeNachricht;

			$Dankesmail->setBody($DankeInhalt);
			$Senden = $Dankesmail->Send();
		}

		// Variable löschen, damit das Formular nicht doppelt abgesendet wird.
		echo GMF_Sicherheit::ersetzeParameterHistory();

	}
}
?>
