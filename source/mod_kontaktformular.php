<?php
/**
 * Kontaktformular! Module Entry Point
 * @since 1.0.0
 * @version 1.1.1
 */


// No direct access
defined('_JEXEC') or die;

use Joomla\CMS\Helper\ModuleHelper;

// Ist das Framework installiert?
$GMF_Fehler = false;
if(!defined('GMFramework') or version_compare(constant('GMFramework'), '0.0.3') < 0) {
    $GMF_Fehler = true;
    goto GMF_Fehler;
}

// Dokument laden
require_once dirname(__FILE__) . '/src/kontaktformular.php';

// Ein neuer Bibelvers aus der Datenbank lesen
$oKontaktformular = new Kontaktformular($params);

require ModuleHelper::getLayoutPath('mod_kontaktformular');

GMF_Fehler:
if($GMF_Fehler) {
    require ModuleHelper::getLayoutPath('mod_kontaktformular','gmf_error'); 
}  
?>